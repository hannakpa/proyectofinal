const express = require("express"); ///forma de importa como import from, pero al estilo de sequelize.
const cors = require("cors");
const endpointsUsuarios = require("./routes/usuarioRoutes.js");
const app = express(); ///defino que voy a usar express.

app.use(express.json()); //hace que acepte el formato json. sino no aceptaria nada
app.use(cors()); //evita problemas al conectar desde otro servidor
app.use("/api/usuarios", endpointsUsuarios); //luego en el controlador definire esta funcion de registroUsuario, le dar'e la rutaque quiera /ruta y la exportar'e.
//la ruta sera: http://localhost:4000/fotopris/api/usuarios/ + la ruta que defina para loginUsuario (en usuarioRoutes)

//arranque del servidor
const port = 4000;
app.listen(port, () => console.log(`Express en puerto ${port}!`));
