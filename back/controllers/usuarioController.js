const { Usuarios } = require("../models/index");
const jsonwebtoken = require("jsonwebtoken");
//defino las funciones que voy a exportar a routes:

const listaUsuario = (req, res) => {
  Usuarios.findAll()
    .then((lista) =>
      res.json({
        ok: true,
        data: lista,
      })
    )
    .catch((err) =>
      res.json({
        ok: false,
        error: err,
      })
    );
};

const loginUsuario = (req, res) => {
  const response = {};
  const { email, password } = req.body; //estoy definiendo el contenido del cuerpo que se enviar'a.

  /////////////COMPARACION DE LO QUE ENVIO CON LO QUE HAY. TODO IRA BIEN SOLO SI LOS DATOS DE REQ COINCIDEN CON AGUN CAMPO DE LA TABLA DE LA BD.

  ///primero que todo comprueba que no falte ni el email ni el password.//////////
  if (!email || !password) {
    //si no encuentra el email o el password, entonces... (no )
    return res ///devolver esta respuesta
      .status(400) //el tipo de error
      .json({ ok: false, msg: "email o pass no encontrados" }); ///este mensaje en formato .json
  } ////////rermina la comprobacion de existencia///////////

  // QUERY
  Usuarios.findOne({ where: { email } }) ///en la tabla usuarios busca el campo email.
    ////1er paso
    .then((usuario) => {
      console.log(usuario);
      //cogemos el usuario

      if (password == usuario.password) {
        //si existe el usuario y el valor del password coincide con el indicado en la tabla,
        return usuario; ///devolver usuario
      } else {
        throw "El password es incorrecto"; //sino devolver error
      }
    })
    ///2do paso
    .then((usuario) => {
      console.log(usuario);
      response.token = jsonwebtoken.sign(
        //
        {
          email,
          nombre: usuario.nombre,
          id: usuario.id,
        },
        "secretKey"
      );
      response.ok = true;
      res.json(response);
    })
    .catch((error) => {
      res.status(400).json({
        ok: false,
        msg: error,
      });
    });
};

//DESPUES DEL CHECK LOGIN

// const home = (req, res) => {
//   //   const { datosUsuario } = req;
//   //   res.json(datosUsuario);
//   // };

//   // router.get("/:id", (req, res, next) => {
//   const idABuscar = req.body;

//   Usuarios.findOne({
//     where: {
//       id: idABuscar,
//     },
//   })
//     .then((datos) => {
//       delete datos.password;
//       res.status(200).json({ ok: true, data: datos });
//     })
//     .catch((err) => res.status(400).json({ ok: false, error: err.parent.sqlMessage }));
// };

module.exports = { loginUsuario, listaUsuario };
