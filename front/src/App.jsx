//import logo from './logo.svg'
import "./App.css";
import { useState, useEffect } from "react";
import jwt_decode from "jwt-decode";
import AutenticacionContext from "../context/autenticarContext";
import { BrowserRouter, Routes, Route } from "react-router-dom";
//rutas
import Login from "./rutas/public/Login";
import Welcome from "./rutas/public/Welcome";
import HolaUsuario from "./rutas/private/HolaUsuario";
import PublicLayout from "./rutas/public/PublicLayout";
import PrivateLayout from "./rutas/private/PrivateLayout";

function App() {
  const [datosAutenticado, setDatosAutenticado] = useState({});
  const [saveToken, setSaveToken] = useState("");

  useEffect(() => {
    const tokenAlmacenado = localStorage.getItem("token"); //coger el token que haya en localstorage
    if (tokenAlmacenado) {
      setSaveToken(tokenAlmacenado);
    }
  }, []);
  ///ir a la API  y coger los datos.

  useEffect(() => {
    if (saveToken) {
      const decoded = jwt_decode(saveToken);
      setDatosAutenticado(decoded); ////

      localStorage.setItem("token", saveToken);
    } else {
      setDatosAutenticado("");
    }
    ///gohome();
  }, [saveToken]);

  console.log(datosAutenticado);

  return (
    <div>
      <AutenticacionContext.Provider value={{ datosAutenticado, setDatosAutenticado, saveToken, setSaveToken }}>
        <BrowserRouter>
          <Routes>
            {/* ruta general. el index es lo que se vera por defecto. los path son los que cambiarán */}
            <Route path="/" element={<PublicLayout />}>
              <Route index element={<Welcome />} />
              <Route path="login" element={<Login />} />
            </Route>
            {/* ruta para usuario. aqu'i en el appu modificar'en su parte fija tambien */}
            <Route path="/u/" element={<PrivateLayout />}>
              <Route index element={<HolaUsuario />} />
            </Route>
          </Routes>
        </BrowserRouter>
      </AutenticacionContext.Provider>
    </div>
  );
}

export default App;
