import AutenticacionContext from "../../../context/autenticarContext";
import { useContext } from "react";

function HolaUsuario() {
  const { datosAutenticado } = useContext(AutenticacionContext);
  ///cogerdatosAutengicado y . nombre del campo
  return <h1> HOLA USUARIO {datosAutenticado.nombre}</h1>;
}
export default HolaUsuario;
