//import { useState } from "react";
import { Navbar, Nav, Container, Modal, Button, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useContext } from "react";
import { Link, Outlet } from "react-router-dom";
import AutenticacionContext from "../../../context/autenticarContext";

function PrivateLayout() {
  const { saveToken } = useContext(AutenticacionContext);
  const navigateTo = useNavigate();

  const goPublic = () => {
    navigateTo("/login");
  };

  return (
    <>
      {saveToken ? (
        <>
          <Navbar bg="success" variant="success" expand="lg">
            <Container>
              <Link to="/" className="navbar-brand">
                HOME
              </Link>
            </Container>
          </Navbar>
          <Container>
            <Outlet />
          </Container>
        </>
      ) : (
        goPublic()
      )}
    </>
  );
}

export default PrivateLayout;
