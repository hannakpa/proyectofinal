//import { useState, useEffect } from "react";
import { Navbar, Nav, Container, Button, Form, Col, Row } from "react-bootstrap";

import { Link, Outlet } from "react-router-dom";

function PublicLayout() {
  return (
    <div>
      <Container fluid>
        <Row>
          <Navbar bg="dark" variant="dark" expand="lg">
            {" "}
            <Link className="nav-link" to="/">
              <p>Home</p>
            </Link>
          </Navbar>
        </Row>

        {/* ABRE SEGUNDO ROW. COL IZQUIERDA FIJA. COL DERECHA OUTLET CON RUTAS. */}
        <Row>
          <Col xs="12" md="12">
            <div className="big-box">
              <Outlet />
            </div>
          </Col>
        </Row>
      </Container>
      {/* </UserContext.Provider> */}
    </div>
  );
}

export default PublicLayout;
