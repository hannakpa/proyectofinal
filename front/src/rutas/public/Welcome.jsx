import { useNavigate } from "react-router-dom";

function Welcome() {
  const navigateTo = useNavigate();
  const goPrivate = () => {
    navigateTo("/u/");
  };
  return (
    <div className="centrar">
      <h1>PAGINA INICIO</h1>
      <button onClick={goPrivate} className="btn btn-outline-dark">
        Continuar
      </button>
    </div>
  );
}

export default Welcome;
