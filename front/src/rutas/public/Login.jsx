import { Button, Form } from "react-bootstrap";
import { useState, useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { AiOutlineEyeInvisible, AiOutlineEye } from "react-icons/ai";
import Alertas from "./Alertas";
import AutenticacionContext from "../../../context/autenticarContext";
////////FUNCION PRINCIPAL//////

function Login() {
  const { setDatosAutenticado, datosAutenticado, saveToken, setSaveToken } = useContext(AutenticacionContext);
  const navigateTo = useNavigate();

  const [emailclass, setEmailclass] = useState("");
  const [passclass, setPassclass] = useState("");
  const [email, setEmail] = useState("hola@mail.com");
  const [pass1, setPass1] = useState("");
  const [mensaje, setMensaje] = useState("");
  const [ok, setOk] = useState(false);
  const [mostrarPass1, setMostrarPass1] = useState(false);
  const abierto = <AiOutlineEye />;
  const cerrado = <AiOutlineEyeInvisible />;
  const [error, setError] = useState("");
  let iconoOjo1 = abierto;

  if (mostrarPass1 === true) {
    iconoOjo1 = cerrado;
  }

  // const redireccion = () => {
  //   if (saveToken) {
  //     navigateTo("/u/");
  //   } else {
  //     navigateTo("/login");
  //   }
  // };

  const goPrivate = () => {
    navigateTo("/u/");
  };

  // PARA VERIFICAR ANTES DEL ENVIO DEFAULT DEL FORMULARIO. ESTA FUNCION COMPRUEBA ANTES SI SE CUMPLEN ESTAS CONDICIONES.
  function enviarFormulario(e) {
    e.preventDefault();

    let todoOk = true;

    if (!email.includes("@" && ".")) {
      setEmailclass("is-invalid");
      todoOk = false;
    } else {
      setEmailclass(""); //limpia el mensaje
    }

    if (!pass1) {
      setMensaje("Escriba su contraseña");
      setPassclass("is-invalid");
      todoOk = false;
    } else {
      setMensaje("");
    }

    if (todoOk) {
      setOk(true); ///si todo est'a bien le asignamos true al OK
      try {
        const comprobarAPI = async () => {
          const user = {
            email: email,
            password: pass1,
          };
          const url = "http://localhost:4000/api/usuarios/login";

          fetch(url, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(user),
          })
            .then((x) => {
              console.log(x);
              return x.json();
            })
            .then((resultado) => {
              console.log(resultado);
              if (!resultado.ok) {
                setError(`Hay un error: ${resultado.msg}`);
              } else if (resultado.token) {
                localStorage.setItem("token", resultado.token);
                setDatosAutenticado(resultado);
                setSaveToken(resultado.token);
                goPrivate();
              }
            })
            .catch((err) => console.log("hay un error"));
        };
        /////ENVIAR A /HOME SOLO SI COINCIDEN CON LOS DATOS DE LA API. AHORA MANDA A CUALQUIERA.
        comprobarAPI();
        console.log(saveToken);
      } catch (error) {
        console.log("hay un error 2");
      }
    }
  }

  //////////////////////////////////////

  ///CON ESTA FUNCION SE ADJUDICA LO QUE SE ESCRIBE EN EL INPUT AL VALOR.
  //PARA QUE VAYA COMPROBANDO SI CUMPLE CON LAS CONDICIONES SE PONE EL IF AQUI. SI QUIERO QUE COMPRUEBE DESPUES, LO PONGO EN FUNCTION ENVIAR FORMULARIO
  function pasoEmail(e) {
    let x = e.target.value;
    setEmail(x);

    if (!x.includes("@" && ".")) {
      setEmailclass("is-invalid");
    } else {
      setEmailclass("");
    }
  }
  function nuevoPass1(e) {
    let x = e.target.value;
    setPass1(x);
  }

  return (
    <>
      {/* ONSUBMIT--FUNCION ENVIAR FORMULARIO. PARA LA ACCION DE ENVIO POR DEFECTO DEL NAVEGADOR Y ANTES QUE NADA COMPRUEBA LOS DATOS REVISANDO LAS CONDICIONES QUE LES DAMOS  */}
      <Form onSubmit={enviarFormulario}>
        <h4>
          Inicio de sesión<nav></nav>
        </h4>
        {/* ////////////////////////////////////////////////////////////////////////////////////////////// */}

        {error ? <Alertas msg={error} /> : null}
        {/* EMAIL */}
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Correo electrónico</Form.Label>
          <Form.Control className={emailclass} name="email" onInput={pasoEmail} value={email} type="text" placeholder="Escriba su correo electrónico" required />
        </Form.Group>

        {/* PASSWORD 1 */}
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Contraseña</Form.Label>
          <Form.Control type={mostrarPass1 ? "text" : "password"} className={passclass} onInput={nuevoPass1} value={pass1} placeholder="Escriba una contraseña" required></Form.Control>
          <i onClick={() => setMostrarPass1(!mostrarPass1)}>{iconoOjo1}</i>
          <p className="error">{mensaje}</p>
        </Form.Group>

        {/* ////////////////////////////////////////////////////////////////////////// */}

        <Button variant="danger">Cancelar</Button>

        <Button variant="success" type="submit">
          Inicar sesión
        </Button>
      </Form>
    </>
  );
}

export default Login;
